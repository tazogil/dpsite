from configparser import ConfigParser, ParsingError
from glob import glob
from htmlmin import minify
from os.path import isfile, dirname, abspath, basename, join, splitext
from string import Template
from termcolor import colored
from yaml import load, Loader, YAMLError
import lesscpy
import re
import sys
import textile

BASE = dirname(abspath(__file__))
ARTICLES_DIR = join(BASE, "articles")
CONF_FIC = "site.yml"
GABARIT_FIC = "site.html.tpl"
LESS_FIC = "css/dp.less"
GRID_COLS = 4


if not isfile(GABARIT_FIC):
    print(colored("ERREUR: ", "red"),
          "le gabarit du site {} est manquant".format(GABARIT_FIC))
    exit()


with open(GABARIT_FIC, 'r') as fh:
    gabarit = fh.read()


def chargement_configuration():
    """
    Première fonction invoquée.

    Ouvre le fichier de conf site.yml et retourne une liste des pages.

    """
    if not isfile(CONF_FIC):
        print(colored("ERREUR: ", "red"),
              "la configuration du site {} est manquante".format(CONF_FIC))
        exit()
    conf = None
    with open(CONF_FIC, "r") as fh:
        try:
            conf = load(fh, Loader=Loader)
        except YAMLError as e:
            print(colored("ERREUR: ", "red"),
                  "La configuration {} contient des erreurs".format(CONF_FIC))
            print(e)
            exit()

    # les pages du site (nom, mots clés etc...)
    pages = conf.get("pages", [])
    if len(pages) == 0:
        print(colored("ERREUR", "red"),
              "Il manque les pages du site.")
        exit()
    return pages


def verification_conf(pages):
    """
    Vérification de la configuration du projet.

    Le fichier de configuration site.yml décrit l'arborescence des pages
    et des informations clés pour chaque page.

    Vérification de la validité du fichier de conf.
    Pour chaque page:
        - il doit exister un fichier de configuration de la page (sa grille,
        voir plus bas)
        - son titre et ses mots clés sont obligatoires
        - sa description est optionnelle.

    Cette fonction est récursive.


    """
    est_index = lambda slug: "index" if slug == "/" else slug

    for page in pages:
        for (slug, data) in page.items():
            titre = data.get("titre", None)
            pub = data.get("pub", None)
            if pub:
                conf_fic = "articles/{}/conf.ini".format(est_index(slug))
                if not isfile(conf_fic):
                    print(colored("ERREUR", "red"),
                          "Il manque ce fichier de configuration: {}.".format(
                        conf_fic))
                    exit()
                if titre is None:
                    print(colored("ERREUR", "red"),
                          "Il manque un titre à la page {}".format(
                        slug))
                    exit()
                motclefs = data.get("motclefs", None)
                if motclefs is None:
                    print(colored("ERREUR", "red"),
                          "Il manque les mots clés de page {}".format(
                        slug))
                    exit()
                description = data.get("descriptions", "")
                if description == "":
                    print(colored("ATTENTION", "yellow"),
                          "Description manquant à la page {} "
                          "les premières lignes de la page "
                          "seront utilisées".format(slug))
                items = data.get("items", None)
                if items is not None:
                    verification_conf(items)


def lister_slugs(pages, slugs):
    """
    La liste des slugs (identifiant d'url) du site.
    Invoquée pour ne demander que la production de la page passée en
    paramètre.

    """
    for page in pages:
        for (slug, data) in page.items():
            slugs[slug] = data
            items = data.get("items", None)
            if items is not None:
                lister_slugs(items, slugs)
    return slugs


def assembler_article(article, classes):
    """
    Invoquée par assembler_page.

    Un article est rédigé dans /articles/<NOM_PAGE>.
    Son nom est <CR-identifiant>.md :
        C: lettre de la colonne de la grille
        R: chiffre de la rangée de la grilleœ
    Cette fonction convertie son texte en HTML et l'insert
    dans une balise <article></article> en y ajoutant ses classes:
        - celle de sa position dans la grille (a1, c4 par exemple)
        - celles ajoutées par l'auteur pour préciser le style de l'article.

    """
    with open(article, "r") as fh:
        texte = fh.read()
    texte = textile.textile(texte)
    nom = basename(article)
    case = prefixe_article(nom)
    extra_classes = ""
    if classes is not None:
        for (cs, cl) in classes:
            if cs == case:
                extra_classes += "{} ".format(cl[1:])
    return '<article class="{} {}">\n{}\n</article>'.format(case.lower(),
                                                            extra_classes.strip(),
                                                            texte)


def page_seuil(pages):
    """
    Une page ayant des pages-enfants passées en paramètres.

    Cette fonction produit un résumé de chaque sous-page.

    Invoquée par la routine produire.

    """
    html = ""
    for page in pages:
        for (slug, data) in page.items():
            titre = data["titre"]
            html += '<h2><a href="{}.html">{}</a></h2>'.format(slug, titre)
    return textile.textile(html)


def assembler_page(slug):
    """
    Invoquée par produire.

    Une page est constituée d'articles disposés dans une grille (css grid).

    Les articles sont dans /articles/<NOM_PAGE>/

    Le fichier /articles/<NOM_PAGE>/conf.ini définit comment la grille est
    organisée (c'est une valeur possible pour la propriété CSS
    grid-template-areas)


    """

    css = ""
    conf_fic = "articles/{}/conf.ini".format(slug)
    if not isfile(conf_fic):
        print(colored("ERREUR", "red"),
              "Il manque ce fichier de configuration: {}.".format(
            conf_fic))
        exit()
    (css, classes) = produire_css(conf_fic)
    articles = glob("{}/{}/*.md".format(ARTICLES_DIR, slug))
    liste = [assembler_article(art, classes) for art in articles]
    page = "\n".join(liste)
    return (page, css)


def produire_sous_navigation(items, slug_actuel):
    """
    Produire une sous navigation

    """
    retval = ""
    if len(items) > 0:
        retval = '<ul>'
        for page in items:
            for (slug, data) in page.items():
                titre = data.get("titre")
            if slug == slug_actuel:
                retval += '<li>{}</li>'.format(titre)
            else:
                retval += '<li><a href="/{}.html">{}</a></li>'.format(
                    slug, titre)
        retval += '</ul>'
    return retval


def produire_navigation(slug_actuel):
    """
    Production de la navigation du site inclu dans chaque page
    produite par la fonction produire.

    """
    retval = ""
    for page in pages:
        for (slug, data) in page.items():
            titre = data.get("titre")
            items = data.get("items", [])
        snav = produire_sous_navigation(items, slug_actuel)
        slug = "index" if slug == "/" else slug
        # slug_actuel = "index" if slug_actuel == "/" else slug_actuel
        # if slug_actuel == slug:
        #     retval += '<div class="actif" id="{}">'
        #     '<span>{}</span>{}</div>'.format(slug, titre, snav)
        # else:
        smenu = ' class="smenu"' if len(items) > 0 else ''
        lien = lambda l: "" if l == "index" else "{}.html".format(l)
        retval += '<div{} id="{}"><a href="/{}">{}</a>{}</div>'.format(
            smenu, slug, lien(slug), titre, snav)
    return retval


def prefixe_article(article):
    article = basename(article)
    try:
        t = article.index('-')
    except ValueError:
        print(colored("ERREUR: ", "red"), "L'article {} ne respecte pas"
              " la syntaxe attendue.".format(article))
        exit()
        print(article, article[:t])
    return article[:t]


def produire_extras_classes(classes, conf_ini):
    """
    Controle les noms de classes css du ini_path.

    Format :
        <ARTICLE>.<NOM_CLASSE>
    Exemple:
        apropos.cartouche-rouge logique_sujet.cartouche-bleu

    """
    rep = dirname(conf_ini)
    articles = glob("{}/*.md".format(rep))
    articles = [prefixe_article(a) for a in articles]
    classes = classes.split(' ')
    classes = [splitext(classe) for classe in classes]
    prefixes = [classe[0] for classe in classes]
    set1 = set(articles)
    set2 = set(prefixes)
    if not set1 >= set2:
        print(colored("ERREUR", "red"),
              "Problème de nom de classes de {} : "
              " au moins un article prévu dans "
              "les classes n'existe pas "
              " vérifier leurs orthographes".format(conf_ini))
        print(set1, set2)
        exit()
    return classes


def produire_grille(grille, cols, ini_path):
    """
    Contrôle la syntaxe de la grille du ini_path


    """

    items = grille.split('" "')
    items = ['"{}"'.format(item.replace('"', '')) for item in items]
    pat1 = '^".*?"$'
    pat2 = "^([A-Z0-9\.]+\s?){%s}$" % cols
    for item in items:
        if re.match(pat1, item) is None:
            print(colored("ERREUR", "red"),
                  "La grille de {} est invalide : "
                  " manque les apostrophes \"A1 A1 A1\"..."
                  " consulter l'aide.".format(ini_path))
            exit()
        item = item.replace('"', '')
        if re.match(pat2, item) is None:
            print(colored("ERREUR", "red"),
                  "La grille de {} est invalide : "
                  " problèmes de caractères ou la séquence ne correspond pas "
                  " au nombre de colonnes {}".format(ini_path, cols))
            exit()
    rep = dirname(ini_path)
    articles = glob("{}/*.md".format(rep))
    articles = [prefixe_article(a) for a in articles]
    articles_grille = grille.replace('"', '').split(" ")
    articles_grille = [a.strip() for a in articles_grille if a != '.']
    set1 = set(articles)
    set2 = set(articles_grille)
    if not set1 >= set2:
        print(colored("ERREUR", "red"),
              "La grille de {} est invalide : "
              " au moins un article prévu dans "
              "la grille n'existe pas "
              " vérifier leurs orthographes".format(ini_path))
        exit()
    retval = [
        "section.grid{grid-template-areas: %s}" % grille
    ]
    for prefixe in articles:
        retval.append(
            "article.%s{grid-area: %s}" % (prefixe.lower(), prefixe)
        )
    return retval


def produire_css(conf_fic):
    """
    Transformer un fichier de conf.ini en css inséré dans le head>style.

    conf est une chaine multi-lignes (issue de /articles/<NOM_PAGE>/conf.ini):
        - la première ligne est la configuration de la grille de la page
        elle sert de valeur à la propriété css grid-template-areas.
        - les autres lignes sont des noms de classes ajoutées à l'articlepour
        préciser son style.

    """
    retval = list()
    config = ConfigParser()
    try:
        config.read(conf_fic)
    except ParsingError as e:
        print(colored("ERREUR : ", "red"),
              "Le fichier de conf {}"
              " est mal formé, voir l'aide.\n {}".format(conf_fic, e))
        exit()
    try:
        style = config["style"]
    except KeyError:
        print(colored("ATTENTION : ", "yellow"),
              "il manque la section style dans {}".format(conf_fic))
    else:
        cols = style.get("cols", GRID_COLS)
        try:
            cols = int(cols)
        except ValueError:
            print(colored("ERREUR : ", "red"),
                  "le nombre de colonnes (cols) de la grille "
                  "n'est pas un entier, {}".format(conf_fic))
            exit()
        val = cols*'1fr '
        retval.append(
            'section.grid {grid-template-columns: %s}' % val
        )

        classes = style.get("classes", None)
        if classes is not None:
            classes = produire_extras_classes(classes, conf_fic)
        grille = style.get("grille", None)
        if grille is not None:
            grille = produire_grille(grille, cols, conf_fic)
            retval+= grille
        return ("\n".join(retval), classes)


def produire(page):
    """
    Production de page HTML dont le nom de fichier sera slug,

    utilisation du gabarit site.html.tpl

    """
    for (slug, data) in page.items():
        # motclefs = data.get("motclefs", None)
        # description = data.get("descriptions", "")
        pub = data.get("pub")
        titre = data.get("titre")
        items = data.get("items", [])
    nav = produire_navigation(slug)
    slug = "index" if slug == "/" else slug
    (contexte, css) = ("", "")
    if pub:
        (contenu, css) = assembler_page(slug)
        contexte = page_seuil(items)
        classe = slug
        print(colored("PRODUCTION DE", "green"), "{}.html".format(slug))
    else:
        contenu = "<h3 class='en-attente'>Cette page est en rédaction.</h3>"
        classe = "nogrid"
        print(colored("ATTENTION", "yellow"),
              "En rédaction de {}.html".format(slug))
    tpl = Template(gabarit)
    html = tpl.substitute(titre=titre, navigation=nav,
                          contenu=contenu, classe=classe,
                          css=css, contexte=contexte,
                          sans_contexte="sans" if contexte == "" else "")
    html = minify(html)
    with open("{}.html".format(slug), "w") as fh:
        fh.write(html)
    for item in items:
        produire(item)


def produire_less():
    """
    Produit la CSS depuis le fichier .less.

    """
    if not isfile(LESS_FIC):
        print("Le fichier {} est manquant.".format(LESS_FIC))
        exit()
    (nom, ext) = splitext(LESS_FIC)
    css_fic = "{}.css".format(nom)
    css = lesscpy.compile(LESS_FIC, minify=True)
    with open(css_fic, "w") as fh:
        fh.write(css)


# LANCEMENT DE LA MOULINETTE :
tout = True
if "nl" in sys.argv:
    print("Production de la CSS, pas des pages.")
    tout = False

produire_less()
if tout:
    pages = chargement_configuration()
    slugs = lister_slugs(pages, dict())
    verification_conf(pages)
    if len(sys.argv) > 1:
        if "slugs" in sys.argv:
            print(colored("LISTE DES PAGES:", "green"))
            for (s, data) in slugs.items():
                pub = data["pub"]
                p = "index" if s == "/" else s
                print(p, colored("non publiée", "yellow") if not pub else "")
        elif sys.argv[1] in slugs.keys():
            yslug = sys.argv[1]
            page = {yslug: slugs[yslug]}
            produire(page)
    else:
        for page in pages:
            produire(page)
