# Projet d'évolution du site de Dominque Pagani.

## Site statique assisté d'un script produisant les pages HTML depuis des articles MD et maintenu sous GIT.

### Installation


### Principe de mise à jour

Pour changer le site, on le récupére en local depuis son dépôt GIT distant, on fait les modifications, on les pousse vers ce dépôt, on préviens les autres pour qu'ils valident si nécessaire, alors le site en production est mis à jour depuis le dépôt distant. 

Les commandes GIT nécessaires sont présentées plus loin. 

### Usage

En local démarrer le pseudo serveur web à l'aide de la commande @python -m http.server@. Le site est accessible à l'adresse http://0.0.0.0:8080.

Le dossier __articles__ contient le contenu rédactionnel du site. Un article est un morceau de page, son nom se compose du nom de la page suivi d'un tiret,  du nom de l'article et terminé par .md, par exemple @index-apropos.md@ correspond à l'article __apropos__ de la page d'accueil.

Les articles se rédigent dans un simple éditeur de texte (pas word!) en utilisant la syntaxe très facile utilisée dans les wikis. 

La CSS du site est /static/css/dpsite.css. Le rédacteur doit juste connaître le positionnement des articles dans la page avec CSS GRID en s'aidant de l'inspecteur FIREFOX, c'est expliqué plus loin.

 Procédure pour ajouter une page

* Créer les articles de la nouvelle page dans le dossier __articles__ nommés @NOMPAGE-NOMARTICLE.md@. Les noms doivent être écrit en minuscules et en ASCII (a-z). Une page contient un nombre indeterminé d'articles qui doivent être rédigés dans "le langage textile":https://textile-lang.com/doc..

* Ajouter une référence à la nouvelle page dans le fichier de configuration dpsite.yml (voir plus bas).

* Reconstruire le site en lançant la commande @python dp.site.py@ qui produit les pages HTML depuis la configuration et les articles.md.

* Définir le positionnement des articles en CSS par les propriétés GRID (voir plus bas).

* Enregistrer un commit avec git sur la branche principale ou bien au besoin créer une nouvelle branche, pousser la nouvelle version sur le dépôt distant. 
