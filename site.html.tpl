<!DOCTYPE html>
<html>
  <head>
    <title> Association Dominique Pagani - $titre</title>
    <meta  charset="UTF-8">
    <meta name="robots" content="none" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/css/mada.css" rel="stylesheet"> 
    <link href="/css/oswald.css" rel="stylesheet"> 
    <link rel="stylesheet" href="/css/dp.css">
    <style>
    $css 
    </style>
  </head>
  <body>
    <header>
      <div class="titres">
        <h1>Association Dominique Pagani</h1>
        <h2>Ateliers de philosophie</h2>
      </div>
      <nav class="principale">$navigation </nav> 
    </header>
    <section>
      <section class="grid $classe">
      $contenu
      </section>
      <aside class="contexte $sans_contexte">
      $contexte
      </aside>
    </section>
    <footer>
    <nav>$navigation </nav>
    </footer>
  </body>
</html>
