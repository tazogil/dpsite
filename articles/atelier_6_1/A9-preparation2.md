
<hr>

h2(#cloche). Présentation de la  &laquo; La cloche félée &raquo;.
Extrait de _Entre crise et guerre: philosopher ?_

p(info). Atelier philosophique. 2ème année, 2015-2016.

h1. &laquo; La cloche félée &raquo;

Le philosophe ne fait que refléter son temps et sans même s'en apercevoir parfois, comme si le présent historique était l'horizon indépassable de la pensée philosophique.

Donc nous sommes toujours en-deçà de la richesse du présent qu'il peut contenir.

h3. De la difficulté à communiquer dans notre modernité 

Inouis sont les moyens de communication de notre modernité et pourtant, tant de malentendus. Dans "le texte de Franz Kafka &laquo; un message impériale &raquo;":/kafka_un_message_imperial.html un messager a pour mission de porter à un humble un message signé de l'Empereur. Le message lui parviendra-t'il ? &laquo; Et toi tu es à ta fenêtre et tu guettes le message quand vient le soir &raquo;. Le message n'arrivera jamais.

h3. Ce poème de Baudelaire illustre le lien entre cette difficulté et notre horizon historique.

