
<hr>

h2(#modern). Présentation de la modernité. Extrait de _Entre crise et guerre: philosopher ?_

p(info). Atelier philosophique. 2ème année, 2015-2016.

h1. Les _maux_ de la&nbsp;cité s'expriment dans ses _mots_ 

Nous sommes toujours en retard sur notre présent historique, cet horizon du temps présent est clairement exprimé par Hegel :

??Tu ne pourras jamais être plus que ton temps mais au mieux tu seras ton temps??

Complexité des rapports entre crise et philosophie. Le cas de cet individu qui va être mis à mort sous pretexte qu'il affaiblit la croyance aux dieux et qu'il corrompt la jeunesse. Est-ce le questionnement qui mène à la crise ? L'accusation est alors justifiée. Est-ce parce que la société (grecque) est en crise que jaillit le questionnement ? Nous aurions alors à faire à un réveilleur.

Pourtant alors que Socrate est condamné à mort, les Macédoniens affutent leurs épées, il avait donc bien raison d'interroger sa cité.

Alors que le politique avec un grand P frappe à la porte de notre intimité, on assiste à un langage plus dépolitisé que jamais: &laquo; il faut montrer que l'on continue à vivre  &raquo;, &laquo; on en veux à NOS valeurs &raquo; Quel est ce _nos_ ? La société française est-elle si homogène ? Ce _nous_, est-ce un milieu ? De quelles _valeurs_ parlent-on ? De celles de l'OTAN ? 

Symptôme de la politique de l'autruiche[1] : c'est quand la conscience ne veut rien chercher à savoir mais continue plus que jamais dans le discours qui a précédé aux événements qui nous secouent, qu'alors les maux de la cité s'expriment dans les mots de la cité.


p(note). [1] Triple jeu de mot Jacques Lacan.

Cette séquence commence à 3'23 et se termine à 37'40.
