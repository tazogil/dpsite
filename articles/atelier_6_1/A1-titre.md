
h1. La logique du sujet face à la reconstruction du nazisme


Les  divers liens, documents, articles ou vidéos ci-après, ont pour fonction première d’ajouter tant soit peu de couleurs, de chair et de sang aux interventions orales de Dominique Pagani, lesquelles en moins de 2h chacune ne peuvent toujours éviter l’excessive généralité, ou "l’universel&nbsp;abstrait".

Les uns donnent un accès plus larges aux références citées au cours de l’atelier, étant bien entendu qu’ils viennent ainsi illustrer la pensée des auteurs évoqués,.. et non celle de l’orateur.

D’autres reprennent les vidéos des années antérieures, en précisant le minutage des séquences ayant alors abordé la problématique du sujet, surtout lorsqu’elles sont évoquées en liaison avec la &laquo; Reconstitution du nazisme &raquo;.

Dans tous les cas, ces &laquo; pièces jointes &raquo; devraient contribuer à clarifier ou préciser quelques séquences orales trop allusives, voire elliptiques et surtout mieux faire émerger la cohérence du propos.

* "Présentation du terme de la modernité":#modern
* "Présentation de &laquo; La cloche fêlée &raquo;":#cloche
* "&laquo; Nacht und nebel &raquo;":#nacht
* "&laquo; Je est un autre &raquo;":#autre


