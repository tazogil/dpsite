
h2(info). Atelier Philosophique 6-1

p(info). Première séance de la sixième saison

p(info). Centre Gosciny, le 11 octobre 2019.


h2. Feinte et/ou sincère : l’hésitation à entrer dans le&nbsp;sujet.

h3. N’y-a-t-il pas des matières autrement passionnantes que cet écheveau de sévères abstractions promises par “La logique du sujet”, ou, à l’opposé, le concret le plus innommable, cet horrible fumier sur lequel se reconstitue le nazisme, sous nos yeux, comme jamais depuis sa défaite?

h3. Pourquoi ne pas délivrer plutôt, cette 'Histoire de la musique' réclamée incessamment par les familiers de nos publications, à commencer par un hommage à l’immense soprano lyrico-dramatique Jessye Norman récemment&nbsp;disparue?

h3. Une forfaiture non moins innommable accomplie le 19 septembre dernier au cœur institutionnel de l’Union Européenne en a décidé autrement, en identifiant le bourreau et sa victime, le SS et le “judéo-bolchevique”.

