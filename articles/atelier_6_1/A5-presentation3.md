
h2. &laquo;Et enfin en question&raquo;

p.  Après ce sujet qui chante, voici le sujet qui parle. Nous avons tous commencé à balbutier la notion de sujet sur les bancs de la communale, aux prises avec cette réflexion(ce retour sur soi) sur le logos qui se nomme grammaire en langue scolaire:

h3. Le sujet est celui qui répond à la question “Qui est-ce qui ?” 

h3. Conjonction ou disjonction du QUI ? et du QUOI ?

h3. Le vent est il un sujet ou un objet, lorsque l'on dit qu’il “agite les arbres“ QUI&nbsp;ou&nbsp;QUOI&nbsp;parle ?

h3. La voix du Paysan de Paris, lorsque elle chante, n’est elle, comme le vent, que cette voix ?

