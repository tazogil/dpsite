
Cher Dominique,

Votre dernière intervention est un véritable trésor.

Je m'efforce chaque jour de me rapprocher de cette remarquable sensibilité qui vous permet de caresser les sons, les sens, et d'atteindre le cœur des mots par des détours.

Je n'ai malheureusement pas l'oreille musicale, qui pourtant me serait d'une aide précieuse dans l'exercice de ma fonction.
