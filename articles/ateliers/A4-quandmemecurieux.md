
h3<>. C'est quand même curieux que Pagani préfère les surfaces planes aux formes biscornues. Son esprit est comme un petit écureuil dans la canopée : il ne peut pas tomber, se  rattrapant toujours à la branche d'une citation ou d'un concept.  Et moi, j'observe le spectacle de cette conférence, subjugué par cet animal étrange qui gambade dans la langue.
