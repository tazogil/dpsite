
L'affinité des paroles de Pagani avec mon âme se manifeste par une  ineffable joie latente chaque fois que j'écoute  une de ses conférences.﻿

Magnifique...le magique devient saisissable...Dieu que c'est beau!
