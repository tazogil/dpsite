
Nerval, c'est bien. La littérature, la poésie, c'est bien. Marx, la politique, c'est bien. Mais c'est vraiment lorsqu'enfin Pagani parle de philosophie,
 celle de Kant,   qu'on atteint le sublime. On oublie le maître qui parle et qui subjugue, on s'oublie soi-même, pour enfin voir apparaître les concepts briller de mille feux.﻿
