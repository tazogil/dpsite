
Je me demande si la grande trouvaille de Pagani, ce ne serait pas au fond le concept du "détour". L'introduction d'une certaine quantité d'entropie dans la pensée. On sait bien que mouvement brownien, entropie, dissipation et amortissement sont liés. 

Qui peut monter tout en haut d'une montagne sans une bonne paire de chaussure qui amortit les chocs d'une marche rude et éprouvante ? Pagani ne fait pas dans le "hors-sujet", il fait dans le "déterminisme et l'aléatoire" pour nous aider à réussir à sortir de la caverne.﻿

Il y a dans les discours de Pagani, des instants incroyables. Pagani, le beau-parleur, commence par nous embarquer avec sa culture, ses citations et son phrasé éloquent. On se laisse bercer, c'est plaisant, c'est léger. Et puis parfois,il change de rythme,  il change de ton. Et termine par un silence prolongé, le visage figé et l'œil noir du Corse. Tout à coup, les deux facultés que sont l'entendement et l'imagination se réveillent ! Entre la vision Pagani  et la remémoration de ses deux dernières phrases qui tournent en boucle dans la tête, c'est une véritable effervescence de l'esprit qui se produit alors.

Les conférences de Pagani ont de l'âme !
