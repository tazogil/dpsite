
Ce petit mot, Mr Pagani, d'un provincial Ardéchois de 48 ans, informaticien et scientifique de formation, pour vous remercier.
De quoi ? D'avoir éviter, par un heureux hasard, un oubli.
Oubli de mes origines familiales communistes stalinienne françaises et espagnoles, oubli de la signification de ce que le communisme incarne ou a incarné, oubli de cette culture encore vivante, grâce à vous entre autres, écrasée sous le talon d'acier d'une idéologie omniprésente.

Que ne me suis je pas réveillé plus tôt pour, peut être, avoir l'immense bonheur de vous cotoyer, vous et toute la bande corse, et sentir mon coeur déborder de gratitude dans le ruissellement de vos facondes.

Tant pis, je lirai donc, mais plus n'importe quoi.

Bien à vous
