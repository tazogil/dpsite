
h1. Féminité et communauté chez Hegel

h2. Le rapport de l'esthétique au politique dans le système

h3. Table des matières

* Introduction.

* L'opposition esthétique suprême.

* Les service divin à la gloire de la vérité.

* De la statue à la comédie, ou de Goethe à Solger.

* Conclusion.
