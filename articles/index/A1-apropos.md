
h2. "À&nbsp;propos&nbsp;de Dominique&nbsp;Pagani":/apropos.html

p(lettrine). Sans l'Internet, peu de personnes connaîtraient Dominique Pagani, car il l'avoue "__je peine à l'écrit__" mais il est tellement bon à l'oral ! Dominique Mazuet de "la librairie Tropique":http://librairie-tropique.fr l'a mis face à un camescope il y a quelques années et depuis nous sommes un bon millier d'internautes à suivre ce professeur de philosophie, d'une immense culture, dans ses détours littéraires qui jettent un regard lucide sur notre mordernité.
"La suite...":/apropos.html

