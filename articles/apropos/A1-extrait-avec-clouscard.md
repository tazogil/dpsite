
h1. "Avec  Clouscard":http://editionsdelga.fr/portfolio/francois-de-negroni/

??Me reviens de ce séjour l'ahurissante performance de Dominique, au Chalut. Il a un peu abusé du hasardeux vien gris le Clos Landry - cette vigne dans la mer chère à Jacques Sauvageot, décédé un an plus tôt, en 1997. Dressé sur une table, histrionique, il va nous réciter intégralement __Phèdre__, de mémoire, en jouant tour à tour chacun des personnages. Nous sommes quatre spectateurs médusés, René, Michel, Isabelle Volpajola, notre gaie acolyte de __Corse-Matin__, et moi. Personne, à l'époque, ne filmait tout le monde, en permanence, au moindre battement de cil, comme aujourd'hui. Nous resterons trois, à avoir la scène encore archivée en nos coeurs.??
-- François de Negroni dans __Avec Clouscard__ page 245.

