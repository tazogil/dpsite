
h1. L'essentiel de mon propos

p>. Résumé d'un "entretien filmé déjà ancien qui mérite le nom d'introduction":#video

h2. Parler sans détours ?

Les discours de Marx, du libéralisme ou de la bourgeoisie peuvent se résumer de manière synthétique à des thèmes fondamentaux, ce ne sont pas des paroles singulières réelles. 

Mais plus on se rapproche de l'effectivité du langage, soit par la parole, moins on échappe aux détours.

Alors, où veux-je en arriver, quel est le but poursuivi ? Pardon, mais Hegel écrit dans la préface à &laquo; la Phénoménologie de l'Esprit &raquo;, que si le philosopĥe veut sans détours exprimer clairement l'essentiel de ses résultats, il le réduit à une simple abstraction, à un universel abstrait, pourtant je vais m'y risquer en répondant sans détours à la question &laquo;&nbsp;_Que&nbsp;faire&nbsp;?_ &raquo;.

h2. Voici ma réponse : PLUS JAMAIS ÇA

Socrate, l'homme le plus juste, celui qui défendait le plus les intérêts de la cité a été mis à mort par la cité. Ce scandale prend aujourd'hui l'aspect d'une forfaiture : plus jamais quoi ? la peste brune ? Non, la peste rouge. 

Forfaiture que ce concept de _totalitarisme_ qui renvoie dos à dos fascisme et communisme, par lequel Hitler se sent moins seul dans son cachot de l'Histoire. 

Qui se réjouissaient de la chute du mur de Berlin ? Moi et mes camarades de RDA, mais aussi tous les champions du carnage anti-communiste.

Celui qui renvoie dos à dos s'extrait de cette dualité et le tour est joué, la société libérale se trouve blanchie de sa vieille complicité avec le nazisme avant la seconde guerre mondiale : &laquo; plutôt Hitler que le Front Populaire &raquo; lisait-on à l'époque dans la presse libérale.

&laquo; Nous sommes de vrais humanistes parce que nous dénonçons le totalitarisme &raquo; entend-on en boucle sur les médias, mais nous ne sommes pas trois, mais toujours deux.

Les guerres au Moyen-Orient et en Afrique sont les effets de cette forfaiture, ainsi que les guerres &laquo; sociétales &raquo; entre genres, cultures, classes d'ages, mais aussi celles entre chomeurs, salariés du privé, salariés du public. ^1^ 

La crise lève un énorme mécontentement qui ne s'exprime pour raison de censure issue de la guerre froide (universalité et progrès sont des notions discrédités), autrement que dans des termes fantasmés et imaginéaires, en un mot on dit populiste. 

Je m'emploie à démasquer cette forfaiture sur le front idéologique mais par les détours de la parole.

p(note). 1. Faut-il sauver le capital pour sauver la sécu s'interroge B. Friot ?


