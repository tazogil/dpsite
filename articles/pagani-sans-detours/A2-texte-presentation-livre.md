
h1. Pagani sans détours

h2. Un livre d'Alexis Manago publié aux "éditions&nbsp;Delga.":http://editionsdelga.fr/


Dominique Pagani, philosophe et musicologue, a consacré sa vie à faire entrer des milliers d’étudiants en philosophie. Pour la première fois, un recueil de ses cours — dispensés initialement et principalement à l’oral —, a été établi avec rigueur par Alexis Manago.

Instrument unique en son genre, fruit d’une expérience pédagogique éprouvée qui prolonge la démarche progressiste et humaniste de l’université populaire de Georges Politzer, ce manuel emmène le lecteur, étape après étape, dans l’histoire de la pensée et l’étude de ses enjeux contemporains.

Dominique Pagani né en 1945 est philosophe et musicologue. Les éditions Delga ont déjà publié son ouvrage Féminité et communauté chez Hegel. Le rapport de l’esthétique au politique dans le système, et préparent avec lui un opus consacré à l’histoire de la musique.

