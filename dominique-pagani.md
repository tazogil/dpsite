 <section class="grid page">
 <article class="performance">

h2. Performeur

h3. Extrait du désopilant livre de François de Negroni sur Michel Clouscard.

??Me reviens de ce séjour l'ahurissante performance de Dominique, au Chalut. Il a un peu abusé du hasardeux vien gris le Clos Landry - cette vigne dans la mer chère à Jacques Sauvageot, décédé un an plus tôt, en 1997. Dressé sur une table, histrionique, il va nous réciter intégralement __Phèdre__, de mémoire, en jouant tour à tour chacun des personnages. Nous sommes quatre spectateurs médusés, René, Michel, Isabelle Volpajola, notre gaie acolyte de __Corse-Matin__, et moi. Personne, à l'époque, ne filmait tout le monde, en permanence, au moindre battement de cil, comme aujourd'hui. Nous resterons trois, à avoir la scène encore archivée en nos coeurs.??
-- François de Negroni dans __Avec Clouscard__ page 245.

  </article>
  <article class="avec-clouscard">
  
!{width:180px}http://editionsdelga.fr/wp-content/uploads/2013/12/avec-clouscard-francois-de-negroni.jpg!:http://editionsdelga.fr/portfolio/avec-clouscard/

  </article>
  <article class="fausse-note">
  <div class="video">
  <iframe src="https://www.youtube.com/embed/YpKfdOed-0E" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0"></iframe>
  </div>
  </article>
  <article class="musicologue">

h2. Musicologue

h3. La Fausse Note

h3. Avec Dominique Pagani

h3. Réalisation/Montage : Aliocha
  
  </article>
  </section>
